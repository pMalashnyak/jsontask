package com.Malashnyak.JSON;

import com.Malashnyak.JSON.JSONParser.Parser;
import com.Malashnyak.JSON.Models.Sweet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    String json = "src/main/resources/sweetsJSON";
    Parser parser = new Parser();
    List<Sweet> sweets = new ArrayList<Sweet>();
    try {
      sweets = parser.getSweets(json);
    } catch (IOException e) {
      e.printStackTrace();
    }
    for (Sweet sw : sweets) {
      System.out.println(sw.toString());
    }
  }
}
