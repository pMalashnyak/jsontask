package com.Malashnyak.JSON.JSONParser;

import com.Malashnyak.JSON.Models.Sweet;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Parser {
  private Gson gson;

  public Parser() {
    gson = new Gson();
  }

  public List<Sweet> getSweets(String jsonFile) throws IOException {
    List<Sweet> sweets = new ArrayList<Sweet>();
    Type listType = new TypeToken<ArrayList<Sweet>>() {}.getType();
    sweets = gson.fromJson(new FileReader(jsonFile), listType);
    return sweets;
  }
}
